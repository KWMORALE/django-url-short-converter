from django.contrib import admin
from django.urls import path
from short_url import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="Home"),
    path('<str:domain>/<str:short>', views.original, name="Original")
]
