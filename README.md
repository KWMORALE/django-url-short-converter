# Django shorter URL converter

Convert URL source to short version and return it in form <domain>/<shortcut>

The domain, shortcut and URL are stored for administration panel.

## Launching the application

### Docker-compose

Type `docker-compose up` in your bash sheell

### Local installation and run

install packages by `pip install requirements.txt`
run `python manage.py runserver 0.0.0.0:8000`

Server will be available under `0.0.0.0:8000`

## Administration
Go to the source: `0.0.0.0:8000/admin` and log in.

Default login details for the admin account:

login: admin

password: password
