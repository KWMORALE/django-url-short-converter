from django.shortcuts import render, redirect
from urllib.parse import urlparse
from .forms import UrlForm
from .models import Link
from .shortner import shortner


def home(request):
    form = UrlForm()
    long_url = ""
    response = ""
    if request.method == 'POST':
        form = UrlForm(request.POST)
        if form.is_valid():
            long_url = form.cleaned_data['long_url']
            link = Link.objects.filter(long_url=long_url)
            if link:
                short_url = link[0].short_url
                domain_name = link[0].domain_name
                response = domain_name + "/" + short_url
            else:
                new_url = form.save(commit=False)
                domain_name = urlparse(long_url).netloc
                short_url = shortner().issue_token()
                new_url.short_url = short_url
                new_url.domain_name = domain_name
                new_url.save()
                response = domain_name + "/" + short_url

    context = {'form': form, 'response': response, 'url': long_url}
    return render(request, 'home.html', context)


def original(request, domain, short):
    link = Link.objects.filter(domain_name=domain, short_url=short)[0]
    return redirect(link.long_url)
