from django.db import models


class Link(models.Model):
    short_url = models.CharField(max_length=30)
    long_url = models.URLField("URL", unique=False)
    domain_name = models.CharField(max_length=30)

    def __str__(self):
        return self.long_url
